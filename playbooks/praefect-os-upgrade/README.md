# Praefect OS Upgrade

This set of ansible playbooks execute various validation checks and operations required for OS upgrade on Praefect servers.

## Change management plan
A change management plan is required to perform OS upgrade operation. A short summary of steps is:

1. Increase `praefect` servers from 3 to 6
    - New machines will run newer OS version
2. Add machines behind the load balancer and run in "dual mode" (running machines with older OS as well as machines with newer OS)
    - Allow us to check if there are any performance regression
    - Validate that everything works on new OS version
    - Easily rollback to older OS servers
3. Stop and delete old machines to let the servers with new OS version, handle all the requests for some time and validate everything works with new OS version
4. Reprovision the old machines with new OS version and run the extended fleet for some time. At this point all 6 servers will be running new OS version
5. Scale down the fleet back to 3
    - These 3 nodes will be now running new OS version

[This is an example of change management plan](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8575) to upgrade OS version from Ubuntu `16.04` to `20.04` in production and it can be used as a template for new change management issue.

## How-to

### Useful flags

#### --limit old_servers|new_servers
Limits the operation to be performed on either `older_servers` or `new_servers` defined in inventory

#### -e "expected_instances=<num>"
Setting expected number of instances in a given environment to perform automated check to match instance count

#### -e "target_distribution_version=<distribution_version>"
Provide distribution version to check the target instance OS against e.g. 16.04

### -t <some_tag>
To run specific tasks marked with `tags` 
- `create`
- `delete`
- `stop`

### Check GCP resources for Praefect
```
ansible-playbook -i inventory/gstg.yaml playbooks/praefect-os-upgrade/check-gcp-resources.yaml -e "expected_instances=3"
```

### Perform checks on the Praefect Servers
```
ansible-playbook -i inventory/gstg.yaml playbooks/praefect-os-upgrade/perform-server-checks.yaml --limit old_servers -e "target_distribution_version=16.04"
```

Note: `Is server receiving traffic?` task may fail on low-traffic servers e.g. `pre` or `gstg-cny`. It will be ignored to complete the rest of play but you should check the logs and run diagnostics manually on the target servers to ensure that praefect is serving the traffic.

### Create disk snapshots
Update `issue_id` to refer to CR issue ID

```
ansible-playbook -i inventory/gstg.yaml playbooks/praefect-os-upgrade/create-disk-snapshots.yaml -e "issue_id=17302"
```

### Delete disk snapshots
Update `issue_id` to refer to CR issue ID

```
ansible-playbook -i inventory/gstg.yaml playbooks/praefect-os-upgrade/delete-disk-snapshots.yaml -e "issue_id=17302"
```

### Stop instances
```
ansible-playbook -i inventory/gstg.yaml playbooks/praefect-os-upgrade/stop-server.yaml -limit old_servers
```

### Delete instances
```
ansible-playbook -i inventory/gstg.yaml playbooks/praefect-os-upgrade/delete-server.yaml -limit old_servers
```
