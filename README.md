# Praefect Anisble Scripts
This is a collection of ansible helper scripts for various operations on the Praefect servers.

## Pre-requisites

- python3
- jq
- gcloud CLI (Its used by ansible playbooks to run GCP commands locally)

## Environment setup

We use [virtual environments](https://docs.python.org/3/library/venv.html) provided by python3 to install ansible.

```shell
# Create `.venv` dir
python3 -m venv .venv

# Activate virtual env
source ./.venv/bin/activate # if you're a firsh user use ./.venv/bin/activate.fish

# Update pip
pip install --upgrade pip

# Install dependencies
pip install -r requirements.txt

# Validate that things are working
which ansible-playbook
path/to/project/.venv/bin/ansible-playbook

# When done you can deactivate the virtual environment
deactivate
```

## Available operations
- [Praefect OS Upgrade](playbooks/praefect-os-upgrade/README.md)
